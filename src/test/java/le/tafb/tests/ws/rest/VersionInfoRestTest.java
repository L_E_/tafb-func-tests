package le.tafb.tests.ws.rest;

import le.tafb.utils.AbstractRestTest;

/**
 * A container for VersionInfo REST resource related tests.<br/>
 * //TODO: Implement.
 * @author L.E.
 * @since 2013-11-15
 */
public class VersionInfoRestTest extends AbstractRestTest {
    @Override
    protected String getResourceURL() {
        //Need to implement this. See CalcRestTest as an example.
        return null;
    }
}
